const userGames = require("../controller/user_games");
const isAuth = require("../middlewares/isAuth");
const { body } = require("express-validator");
const multer = require("multer");
const storage = require("../config/multerStorage");
const upload = multer({
    storage: storage,
    fileFilter: (req, file, cb) => {
        if (file.mimetype === "video/mkv" || file.mimetype === "video/mp4") {
            cb(null, true);
        } else {
            cb({
                    status: 400,
                    message: "Invalid file type",
                },
                false
            );
        }
    },
});
const router = require("express").Router();

router.get("/user-games", isAuth, userGames.getUserGames);

router.post(
    "/user-game",
    upload.single("video"), [
        body("email").isEmail().not().isEmpty().withMessage("Email is required"),
        body("password").not().isEmpty().withMessage("Password is required"),
        body("video").custom((value, { req }) => {
            if (req.file) {
                return true;
            }
            throw new Error("Video is required");
        }),
    ],
    userGames.postUserGame
);

router.put(
    "/user-game/:id",
    upload.single("video"),
    isAuth,
    userGames.putUserGame
);

router.delete("/user-game/:id", isAuth, userGames.deleteUserGame);

router.post("/login", userGames.postLogin);

router.post("/forgot-password", userGames.postSendOTP);

router.post("/verify-forgot-password", userGames.postInputOTP);

router.post("/change-password", userGames.postChangePassword);

module.exports = router;