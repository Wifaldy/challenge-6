const userGameBiodatas = require("../controller/user_game_biodatas");
const isAuth = require("../middlewares/isAuth");
const router = require("express").Router();

router.get("/user-game-biodatas", isAuth, userGameBiodatas.getUserGameBiodatas);

router.post("/user-game-biodata", isAuth, userGameBiodatas.postUserGameBiodata);

router.put(
    "/user-game-biodata/:id",
    isAuth,
    userGameBiodatas.putUserGameBiodata
);

router.delete(
    "/user-game-biodata/:id",
    isAuth,
    userGameBiodatas.deleteUserGameBiodata
);

module.exports = router;