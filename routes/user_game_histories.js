const userGameHistories = require("../controller/user_game_histories");
const isAuth = require("../middlewares/isAuth");
const { body } = require("express-validator");

const router = require("express").Router();

router.get(
    "/user-game-histories",
    isAuth,
    userGameHistories.getUserGameHistories
);

router.post(
    "/user-game-history",
    isAuth, [body("score").not().isEmpty().withMessage("Score is required")],
    userGameHistories.postUserGameHistory
);

router.put(
    "/user-game-history/:id",
    isAuth,
    userGameHistories.putUserGameHistory
);

router.delete(
    "/user-game-history/:id",
    isAuth,
    userGameHistories.deleteUserGameHistory
);

module.exports = router;