const { user_game_histories } = require("../models");
const { validationResult } = require("express-validator");

exports.getUserGameHistories = async(req, res, next) => {
    try {
        const userGames = await user_game_histories.findAll({
            where: {
                user_game_id: req.user.id,
            },
        });
        res.status(200).json({ user_game_histories: userGames });
    } catch (error) {
        next(error);
    }
};

exports.postUserGameHistory = async(req, res, next) => {
    const { score } = req.body;
    try {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            throw {
                status: 400,
                message: errors.array()[0].msg,
            };
        }
        const userGame = await user_game_histories.create({
            score: score,
            time: new Date(),
            user_game_id: req.user.id,
            createdAt: new Date(),
            updatedAt: new Date(),
        });
        res.status(201).json({ user_game_history: userGame });
    } catch (error) {
        next(error);
    }
};

exports.putUserGameHistory = async(req, res, next) => {
    const { id } = req.params;
    try {
        let userGame = await user_game_histories.findByPk(id);
        if (!userGame) {
            throw {
                status: 404,
                message: "User game history not found",
            };
        }
        if (userGame.user_game_id !== req.user.id) {
            throw {
                status: 401,
                message: "Unauthorized request",
            };
        }
        userGame = await user_game_histories.update({
            ...req.body,
            updatedAt: new Date(),
        }, {
            where: {
                id: id,
            },
            returning: true,
        });
        res.status(200).json({ user_game_history: userGame[1][0] });
    } catch (error) {
        next(error);
    }
};

exports.deleteUserGameHistory = async(req, res, next) => {
    const { id } = req.params;
    try {
        const userGame = await user_game_histories.findByPk(id);
        if (!userGame) {
            throw {
                status: 404,
                message: "User game history not found",
            };
        }
        if (userGame.user_game_id !== req.user.id) {
            throw {
                status: 401,
                message: "Unauthorized request",
            };
        }
        await userGame.destroy();
        res.status(200).json({ message: "User game history successfully deleted" });
    } catch (error) {
        next(error);
    }
};