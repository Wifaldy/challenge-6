require("dotenv").config();
const {
    user_games,
    user_game_biodatas,
    user_game_histories,
} = require("../models");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");
const { validationResult } = require("express-validator");
const { OAuth2Client } = require("google-auth-library");
const client = new OAuth2Client(
    process.env.GOOGLE_CLIENT_ID,
    process.env.GOOGLE_CLIENT_SECRET
);
const cloudinary = require("../config/cloudinary");
const fs = require("fs");
const sendMail = require("../config/sendMail");
const otp = require("otp-generator");

exports.getUserGames = async(req, res, next) => {
    try {
        const userGames = await user_games.findAll();
        res.status(200).json({ user_games: userGames });
    } catch (error) {
        next(error);
    }
};

exports.postUserGame = async(req, res, next) => {
    try {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            throw {
                status: 400,
                message: errors.array()[0].msg,
            };
        }
        const { email, password } = req.body;
        const video = await cloudinary.uploader.upload(req.file.path, {
            resource_type: "video",
        });
        fs.unlinkSync(req.file.path);
        const user = await user_games.findOne({
            where: {
                email: email,
            },
        });
        if (user) {
            throw {
                status: 400,
                message: "Email already exists",
            };
        }
        const userGame = await user_games.create({
            email: email,
            password: password,
            video: video.secure_url,
            createdAt: new Date(),
            updatedAt: new Date(),
        });
        await sendMail(
            "hacktigo@gmail.com",
            userGame.email,
            "Registration",
            "You have successfully registered"
        );
        res.status(201).json({ user_game: userGame });
    } catch (error) {
        next(error);
    }
};

exports.putUserGame = async(req, res, next) => {
    const { id } = req.params;
    try {
        let userGame = await user_games.findByPk(id);
        if (!userGame) {
            throw {
                status: 404,
                message: "User game not found",
            };
        }
        const hash = await bcrypt.hash(req.body.password, 12);
        const video = await cloudinary.uploader.upload(req.file.path, {
            resource_type: "video",
        });
        fs.unlinkSync(req.file.path);
        userGame = await user_games.update({
            password: hash,
            video: video.secure_url,
            updatedAt: new Date(),
        }, {
            where: {
                id: id,
            },
            returning: true,
        });
        res.status(200).json({ user_game: userGame[1][0] });
    } catch (error) {
        next(error);
    }
};

exports.deleteUserGame = async(req, res, next) => {
    const { id } = req.params;
    try {
        const userGame = await user_games.findByPk(id);
        if (!userGame) {
            throw {
                status: 404,
                message: "User game not found",
            };
        }
        await user_game_biodatas.destroy({
            where: {
                user_game_id: userGame.id,
            },
        });
        await user_game_histories.destroy({
            where: {
                user_game_id: userGame.id,
            },
        });
        await userGame.destroy();
        res.status(200).json({ message: "User game deleted" });
    } catch (error) {
        next(error);
    }
};

exports.postLogin = async(req, res, next) => {
    const { email, password, google_id_token } = req.body;
    try {
        if (email || password) {
            const userGame = await user_games.findOne({
                where: {
                    email: email,
                },
            });
            if (!userGame) {
                throw {
                    status: 404,
                    message: "User not found",
                };
            }
            const isPasswordValid = await bcrypt.compare(password, userGame.password);
            if (!isPasswordValid) {
                throw {
                    status: 401,
                    message: "Invalid email / password",
                };
            }
            const token = jwt.sign({
                    id: userGame.id,
                    email: userGame.email,
                },
                process.env.JWT_SECRET
            );
            res.status(200).json({ user_game: userGame.id, token });
        } else if (google_id_token) {
            const payload = await client.verifyIdToken({
                idToken: google_id_token,
                audience: process.env.GOOGLE_CLIENT_IDs,
            });
            const email = payload.getPayload().email;
            const user = await user_games.findOne({
                where: {
                    email: email,
                },
            });
            if (!user) {
                const createUser = await user_games.create({
                    email: email,
                    createdAt: new Date(),
                });
                await sendMail(
                    "hacktigo@gmail.com",
                    createUser.email,
                    "Registration",
                    "You have successfully registered"
                );
                const token = jwt.sign({
                        id: createUser.id,
                        email: createUser.email,
                    },
                    process.env.JWT_SECRET
                );
                return res.status(200).json({ token });
            }
            const token = jwt.sign({
                    id: user.id,
                    email: user.email,
                },
                process.env.JWT_SECRET
            );
            res.status(200).json({ token });
        }
    } catch (error) {
        next(error);
    }
};

exports.postSendOTP = async(req, res, next) => {
    try {
        const { email } = req.body;
        const user = await user_games.findOne({
            where: {
                email: email,
            },
        });
        if (!user) {
            throw {
                status: 404,
                message: "User not found",
            };
        }
        const otpGenerate = otp.generate(6, {
            upperCaseAlphabets: false,
            specialChars: false,
        });
        const hashOTP = await bcrypt.hash(otpGenerate, 12);
        await user_games.update({
            otp: hashOTP,
            updatedAt: new Date(),
            otp_expiry: new Date(new Date().getTime() + 3600000),
        }, {
            where: {
                email: email,
            },
        });
        const html = `
        <h2>Token anda : ${otpGenerate}</h2>
        <p>Token ini hanya berlaku 1 jam</p>
        `;
        await sendMail("hacktigo@gmail.com", email, "Forgot Password", null, html);
        res.status(200).json({ message: "OTP has been sent" });
    } catch (err) {
        next(err);
    }
};

exports.postInputOTP = async(req, res, next) => {
    try {
        const { email, otp } = req.body;
        const user = await user_games.findOne({
            where: {
                email: email,
            },
        });
        if (!user) {
            throw {
                status: 404,
                message: "User not found",
            };
        }
        const isOTPValid = await bcrypt.compare(otp, user.otp);
        if (!isOTPValid) {
            throw {
                status: 401,
                message: "Invalid OTP",
            };
        }
        if (new Date() > user.otp_expiry) {
            throw {
                status: 401,
                message: "OTP expired",
            };
        }
        res.status(200).json({ message: "OTP valid" });
    } catch (err) {
        next(err);
    }
};

exports.postChangePassword = async(req, res, next) => {
    try {
        const { email, confPassword, password, otp } = req.body;
        const user = await user_games.findOne({
            where: {
                email: email,
            },
        });
        if (!user) {
            throw {
                status: 404,
                message: "User not found",
            };
        }
        const isOTPValid = await bcrypt.compare(otp, user.otp);
        if (!isOTPValid) {
            throw {
                status: 401,
                message: "Invalid OTP",
            };
        }
        if (new Date() > user.otp_expiry) {
            throw {
                status: 401,
                message: "OTP expired",
            };
        }
        if (password !== confPassword) {
            throw {
                status: 401,
                message: "Password not match",
            };
        }
        const hashPassword = await bcrypt.hash(password, 12);
        await user_games.update({
            password: hashPassword,
            updatedAt: new Date(),
            otp: null,
            otp_expiry: null,
        }, {
            where: {
                email: email,
            },
        });
        res.status(200).json({ message: "Password changed" });
    } catch (err) {
        next(err);
    }
};