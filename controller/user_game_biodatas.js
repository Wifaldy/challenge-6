const { user_game_biodatas } = require("../models");

exports.getUserGameBiodatas = async(req, res, next) => {
    try {
        const userGames = await user_game_biodatas.findOne({
            where: {
                user_game_id: req.user.id,
            },
        });
        if (!userGames) {
            throw {
                status: 404,
                message: "User game biodata not found",
            };
        }
        res.status(200).json({ user_game_biodatas: userGames });
    } catch (error) {
        next(error);
    }
};

exports.postUserGameBiodata = async(req, res, next) => {
    try {
        let userGame = await user_game_biodatas.findOne({
            where: {
                user_game_id: req.user.id,
            },
        });
        if (userGame) {
            throw {
                status: 400,
                message: "User game biodata already exists",
            };
        }
        userGame = await user_game_biodatas.create({
            ...req.body,
            user_game_id: req.user.id,
            createdAt: new Date(),
            updatedAt: new Date(),
        });
        res.status(201).json({ user_game_biodata: userGame });
    } catch (error) {
        next(error);
    }
};

exports.putUserGameBiodata = async(req, res, next) => {
    const { id } = req.params;
    try {
        let userGame = await user_game_biodatas.findByPk(id);
        if (!userGame) {
            throw {
                status: 404,
                message: "User game biodata not found",
            };
        }
        userGame = await user_game_biodatas.update({
            ...req.body,
            updatedAt: new Date(),
        }, {
            where: {
                id: id,
            },
            returning: true,
        });
        res.status(200).json({ user_game_biodata: userGame[1][0] });
    } catch (error) {
        next(error);
    }
};

exports.deleteUserGameBiodata = async(req, res, next) => {
    const { id } = req.params;
    try {
        const userGame = await user_game_biodatas.findByPk(id);
        if (!userGame) {
            throw {
                status: 404,
                message: "User game biodata not found",
            };
        }
        await userGame.destroy();
        res.status(200).json({ message: "User game biodata successfully deleted" });
    } catch (error) {
        next(error);
    }
};