const swaggerAutogen = require('swagger-autogen')();

const output = "./swagger.json";
const endpoint = ['./routes/user_game_biodatas', './routes/user_game_histories', './routes/user_games'];

swaggerAutogen(output, endpoint)
    .then(() => {
        console.log('swagger.json generated');
    });