const express = require("express");
const userGamesRouter = require("./routes/user_games");
const userGameBiodatasRouter = require("./routes/user_game_biodatas");
const userGameHistoriesRouter = require("./routes/user_game_histories");
const errorHandler = require("./errorHandler");
const morgan = require("morgan");
const app = express();

const swaggerUi = require("swagger-ui-express");
const swaggerDocument = require("./swagger.json");

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use(morgan("tiny"));
app.use("/api-doc", swaggerUi.serve, swaggerUi.setup(swaggerDocument));
app.use(userGamesRouter);
app.use(userGameBiodatasRouter);
app.use(userGameHistoriesRouter);
app.use(errorHandler);

module.exports = app;