"use strict";
const { Model } = require("sequelize");
const bcrypt = require("bcryptjs");
module.exports = (sequelize, DataTypes) => {
    class user_games extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
            user_games.hasOne(models.user_game_biodatas, {
                foreignKey: "user_game_id",
            });
            user_games.hasMany(models.user_game_histories, {
                foreignKey: "user_game_id",
            });
        }
    }
    user_games.init({
        email: DataTypes.STRING,
        password: DataTypes.STRING,
        video: DataTypes.STRING,
        otp: DataTypes.STRING,
        otp_expiry: DataTypes.DATE,
    }, {
        sequelize,
        modelName: "user_games",
    });

    user_games.beforeCreate(async(user_games, options) => {
        if (user_games.password) {
            user_games.password = await bcrypt.hash(user_games.password, 12);
        }
    });
    return user_games;
};