'use strict';
const {
    Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class user_game_biodatas extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
            user_game_biodatas.belongsTo(models.user_games, {
                foreignKey: 'user_game_id',
            });

        }
    }
    user_game_biodatas.init({
        fullname: DataTypes.STRING,
        email: DataTypes.STRING,
        birth_date: DataTypes.DATE,
        user_game_id: DataTypes.INTEGER
    }, {
        sequelize,
        modelName: 'user_game_biodatas',
    });
    return user_game_biodatas;
};