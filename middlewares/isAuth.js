const jwt = require("jsonwebtoken");

module.exports = async(req, res, next) => {
    try {
        const authHeader = req.get("Authorization");
        if (!authHeader) {
            throw {
                status: 401,
                message: "Token not provided",
            };
        }
        const decodedToken = jwt.verify(authHeader, process.env.JWT_SECRET);
        req.user = decodedToken;
        next();
    } catch (error) {
        next(error);
    }
};