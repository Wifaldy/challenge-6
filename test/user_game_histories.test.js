const request = require("supertest");
const { sequelize } = require("../models");
const { queryInterface } = sequelize;
const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");
const app = require("../app");

let token;
let token2;

beforeEach(async() => {
    const hash = await bcrypt.hash("123", 12);
    await queryInterface.bulkInsert("user_games", [{
            username: "test",
            password: hash,
            createdAt: new Date(),
            updatedAt: new Date(),
        },
        {
            username: "test2",
            password: hash,
            createdAt: new Date(),
            updatedAt: new Date(),
        },
    ]);
    await queryInterface.bulkInsert("user_game_histories", [{
        score: 1,
        time: new Date(),
        user_game_id: 1,
        createdAt: new Date(),
        updatedAt: new Date(),
    }, ]);
    token = jwt.sign({
            id: 1,
            username: "test",
        },
        "usergamesecretsecret"
    );
    token2 = jwt.sign({
            id: 2,
            username: "test2",
        },
        "usergamesecretsecret"
    );
});

afterEach(async() => {
    await queryInterface.bulkDelete(
        "user_games", {}, { truncate: true, restartIdentity: true }
    );
    await queryInterface.bulkDelete(
        "user_game_histories", {}, { truncate: true, restartIdentity: true }
    );
});

describe("GET /user-game-histories", () => {
    it("success", (done) => {
        request(app)
            .get("/user-game-histories")
            .set("authorization", token)
            .expect(200)
            .end((err, res) => {
                if (err) {
                    return done(err);
                }
                expect(res.status).toBe(200);
                expect(res.body).toHaveProperty("user_game_histories");
                expect(res.body.user_game_histories[0]).toHaveProperty("id");
                expect(res.body.user_game_histories[0]).toHaveProperty("score");
                expect(res.body.user_game_histories[0]).toHaveProperty("time");
                expect(res.body.user_game_histories[0]).toHaveProperty("user_game_id");
                expect(res.body.user_game_histories[0]).toHaveProperty("createdAt");
                expect(res.body.user_game_histories[0]).toHaveProperty("updatedAt");
                done();
            });
    });
    it("no auth", (done) => {
        request(app)
            .get("/user-game-histories")
            .end((err, res) => {
                if (err) {
                    return done(err);
                }
                expect(res.status).toBe(401);
                expect(res.body).toHaveProperty("message");
                expect(res.body.message).toBe("Token not provided");
                done();
            });
    });
});

describe("POST /user-game-history", () => {
    it("success", (done) => {
        request(app)
            .post("/user-game-history")
            .set("authorization", token)
            .send({
                score: 1,
            })
            .end((err, res) => {
                if (err) {
                    return done(err);
                }
                expect(res.status).toBe(201);
                expect(res.body).toHaveProperty("user_game_history");
                expect(res.body.user_game_history).toHaveProperty("id");
                expect(res.body.user_game_history).toHaveProperty("score");
                expect(res.body.user_game_history).toHaveProperty("time");
                expect(res.body.user_game_history).toHaveProperty("user_game_id");
                expect(res.body.user_game_history).toHaveProperty("createdAt");
                expect(res.body.user_game_history).toHaveProperty("updatedAt");
                done();
            });
    });
    it("no auth", (done) => {
        request(app)
            .post("/user-game-history")
            .send({
                score: 1,
            })
            .end((err, res) => {
                if (err) {
                    return done(err);
                }
                expect(res.status).toBe(401);
                expect(res.body).toHaveProperty("message");
                expect(res.body.message).toBe("Token not provided");
                done();
            });
    });
    it("Field violation", (done) => {
        request(app)
            .post("/user-game-history")
            .set("authorization", token)
            .end((err, res) => {
                if (err) {
                    return done(err);
                }
                expect(res.status).toBe(400);
                expect(res.body).toHaveProperty("message");
                expect(res.body.message).toBe("Score is required");
                done();
            });
    });
});

describe("PUT /user-game-history", () => {
    it("success", (done) => {
        request(app)
            .put("/user-game-history/1")
            .set("authorization", token)
            .send({
                score: 5,
            })
            .end((err, res) => {
                if (err) {
                    return done(err);
                }
                expect(res.status).toBe(200);
                expect(res.body).toHaveProperty("user_game_history");
                expect(res.body.user_game_history).toHaveProperty("id");
                expect(res.body.user_game_history).toHaveProperty("score");
                expect(res.body.user_game_history).toHaveProperty("time");
                expect(res.body.user_game_history).toHaveProperty("user_game_id");
                expect(res.body.user_game_history).toHaveProperty("createdAt");
                expect(res.body.user_game_history).toHaveProperty("updatedAt");
                done();
            });
    });
    it("not found", (done) => {
        request(app)
            .put("/user-game-history/3")
            .set("authorization", token)
            .send({
                score: 5,
            })
            .end((err, res) => {
                if (err) {
                    return done(err);
                }
                expect(res.status).toBe(404);
                expect(res.body).toHaveProperty("message");
                expect(res.body.message).toBe("User game history not found");
                done();
            });
    });
    it("no auth", (done) => {
        request(app)
            .put("/user-game-history/1")
            .send({
                score: 5,
            })
            .end((err, res) => {
                if (err) {
                    return done(err);
                }
                expect(res.status).toBe(401);
                expect(res.body).toHaveProperty("message");
                expect(res.body.message).toBe("Token not provided");
                done();
            });
    });
    it("unauthorized", (done) => {
        request(app)
            .put("/user-game-history/1")
            .set("authorization", token2)
            .send({
                score: 5,
            })
            .end((err, res) => {
                if (err) {
                    return done(err);
                }
                expect(res.status).toBe(401);
                expect(res.body).toHaveProperty("message");
                expect(res.body.message).toBe("Unauthorized request");
                done();
            });
    });
});

describe("DELETE /user-game-history", () => {
    it("success", (done) => {
        request(app)
            .delete("/user-game-history/1")
            .set("authorization", token)
            .end((err, res) => {
                if (err) {
                    return done(err);
                }
                expect(res.status).toBe(200);
                expect(res.body).toHaveProperty("message");
                expect(res.body.message).toBe("User game history successfully deleted");
                done();
            });
    });
    it("not found", (done) => {
        request(app)
            .delete("/user-game-history/3")
            .set("authorization", token)
            .end((err, res) => {
                if (err) {
                    return done(err);
                }
                expect(res.status).toBe(404);
                expect(res.body).toHaveProperty("message");
                expect(res.body.message).toBe("User game history not found");
                done();
            });
    });

    it("unauthorized", (done) => {
        request(app)
            .delete("/user-game-history/1")
            .set("authorization", token2)
            .end((err, res) => {
                if (err) {
                    return done(err);
                }
                expect(res.status).toBe(401);
                expect(res.body).toHaveProperty("message");
                expect(res.body.message).toBe("Unauthorized request");
                done();
            });
    });
});