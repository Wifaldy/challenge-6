const request = require("supertest");
const { sequelize } = require("../models");
const { queryInterface } = sequelize;
const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");
const app = require("../app");

let token;

beforeEach(async() => {
    const hash = await bcrypt.hash("123", 12);
    await queryInterface.bulkInsert("user_games", [{
            username: "test",
            password: hash,
            createdAt: new Date(),
            updatedAt: new Date(),
        },
        // {
        //     username: "test2",
        //     password: hash,
        //     createdAt: new Date(),
        //     updatedAt: new Date(),
        // },
    ]);
    token = jwt.sign({
            id: 1,
            username: "test",
        },
        "usergamesecretsecret"
    );
});

afterEach(async() => {
    await queryInterface.bulkDelete(
        "user_games", {}, { truncate: true, restartIdentity: true }
    );
});

describe("GET /user-games", () => {
    it("success", (done) => {
        request(app)
            .get("/user-games")
            .set("authorization", token)
            .end((err, res) => {
                if (err) {
                    return done(err);
                }
                expect(res.status).toBe(200);
                expect(res.body).toHaveProperty("user_games");
                expect(res.body.user_games).toHaveLength(1);
                expect(res.body.user_games[0]).toHaveProperty("id");
                expect(res.body.user_games[0]).toHaveProperty("username");
                expect(res.body.user_games[0]).toHaveProperty("password");
                expect(res.body.user_games[0]).toHaveProperty("createdAt");
                expect(res.body.user_games[0]).toHaveProperty("updatedAt");
                done();
            });
    });
    it("no auth", (done) => {
        request(app)
            .get("/user-games")
            .end((err, res) => {
                if (err) {
                    return done(err);
                }
                expect(res.status).toBe(401);
                expect(res.body).toHaveProperty("message");
                expect(res.body.message).toBe("Token not provided");
                done();
            });
    });
});

describe("POST /user-game", () => {
    it("success", (done) => {
        request(app)
            .post("/user-game")
            .send({
                username: "test2",
                password: "123",
            })
            .end((err, res) => {
                if (err) {
                    return done(err);
                }
                expect(res.status).toBe(201);
                expect(res.body).toHaveProperty("user_game");
                expect(res.body.user_game).toHaveProperty("id");
                expect(res.body.user_game).toHaveProperty("username");
                expect(res.body.user_game).toHaveProperty("password");
                expect(res.body.user_game).toHaveProperty("createdAt");
                expect(res.body.user_game).toHaveProperty("updatedAt");
                done();
            });
    });
    it("Field Violation", (done) => {
        request(app)
            .post("/user-game")
            .send({
                username: "test3",
            })
            .end((err, res) => {
                if (err) {
                    return done(err);
                }
                expect(res.status).toBe(400);
                expect(res.body).toHaveProperty("message");
                expect(res.body.message).toBe("Password is required");
                done();
            });
    });
    it("user already exists", (done) => {
        request(app)
            .post("/user-game")
            .send({
                username: "test",
                password: "123",
            })
            .end((err, res) => {
                if (err) {
                    return done(err);
                }
                expect(res.status).toBe(400);
                expect(res.body).toHaveProperty("message");
                expect(res.body.message).toBe("Username already exists");
                done();
            });
    });
});

describe("PUT /user-game/:id", () => {
    it("success", (done) => {
        request(app)
            .put("/user-game/1")
            .set("authorization", token)
            .send({
                password: "123",
            })
            .end((err, res) => {
                if (err) {
                    return done(err);
                }
                expect(res.status).toBe(200);
                expect(res.body).toHaveProperty("user_game");
                expect(res.body.user_game).toHaveProperty("id");
                expect(res.body.user_game).toHaveProperty("username");
                expect(res.body.user_game).toHaveProperty("password");
                expect(res.body.user_game).toHaveProperty("createdAt");
                expect(res.body.user_game).toHaveProperty("updatedAt");
                done();
            });
    });
    it("no auth", (done) => {
        request(app)
            .put("/user-game/1")
            .send({
                password: "123",
            })
            .end((err, res) => {
                if (err) {
                    return done(err);
                }
                expect(res.status).toBe(401);
                expect(res.body).toHaveProperty("message");
                expect(res.body.message).toBe("Token not provided");
                done();
            });
    });
    it("not found", (done) => {
        request(app)
            .put("/user-game/2")
            .set("authorization", token)
            .send({
                password: "123",
            })
            .end((err, res) => {
                if (err) {
                    return done(err);
                }
                expect(res.status).toBe(404);
                expect(res.body).toHaveProperty("message");
                expect(res.body.message).toBe("User game not found");
                done();
            });
    });
});

describe("DELETE /user-game/:id", () => {
    it("success", (done) => {
        request(app)
            .delete("/user-game/1")
            .set("authorization", token)
            .end((err, res) => {
                if (err) {
                    return done(err);
                }
                expect(res.status).toBe(200);
                expect(res.body).toHaveProperty("message");
                expect(res.body.message).toBe("User game deleted");
                done();
            });
    });
    it("no auth", (done) => {
        request(app)
            .delete("/user-game/1")
            .end((err, res) => {
                if (err) {
                    return done(err);
                }
                expect(res.status).toBe(401);
                expect(res.body).toHaveProperty("message");
                expect(res.body.message).toBe("Token not provided");
                done();
            });
    });
    it("not found", (done) => {
        request(app)
            .delete("/user-game/2")
            .set("authorization", token)
            .end((err, res) => {
                if (err) {
                    return done(err);
                }
                expect(res.status).toBe(404);
                expect(res.body).toHaveProperty("message");
                expect(res.body.message).toBe("User game not found");
                done();
            });
    });
});

describe("POST /login", () => {
    it("success", (done) => {
        request(app)
            .post("/login")
            .send({
                username: "test",
                password: "123",
            })
            .end((err, res) => {
                if (err) {
                    return done(err);
                }
                expect(res.status).toBe(200);
                expect(res.body).toHaveProperty("token");
                done();
            });
    });
    it("password invalid", (done) => {
        request(app)
            .post("/login")
            .send({
                username: "test",
                password: "1234",
            })
            .end((err, res) => {
                if (err) {
                    return done(err);
                }
                expect(res.status).toBe(401);
                expect(res.body).toHaveProperty("message");
                expect(res.body.message).toBe("Invalid username / password");
                done();
            });
    });
    it("User not found", (done) => {
        request(app)
            .post("/login")
            .send({
                username: "test2",
                password: "123",
            })
            .end((err, res) => {
                if (err) {
                    return done(err);
                }
                expect(res.status).toBe(404);
                expect(res.body).toHaveProperty("message");
                expect(res.body.message).toBe("User not found");
                done();
            });
    });
});