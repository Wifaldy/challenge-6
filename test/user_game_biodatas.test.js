const request = require("supertest");
const { sequelize } = require("../models");
const { queryInterface } = sequelize;
const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");
const app = require("../app");

let token;
let token2;

beforeEach(async() => {
    const hash = await bcrypt.hash("123", 12);
    await queryInterface.bulkInsert("user_games", [{
            username: "test",
            password: hash,
            createdAt: new Date(),
            updatedAt: new Date(),
        },
        {
            username: "test2",
            password: hash,
            createdAt: new Date(),
            updatedAt: new Date(),
        },
    ]);
    await queryInterface.bulkInsert("user_game_biodatas", [{
        fullname: "test",
        email: "test@test.com",
        birth_date: "2020-01-01",
        user_game_id: 1,
        createdAt: new Date(),
        updatedAt: new Date(),
    }, ]);
    token = jwt.sign({
            id: 1,
            username: "test",
        },
        "usergamesecretsecret"
    );
    token2 = jwt.sign({
            id: 2,
            username: "test2",
        },
        "usergamesecretsecret"
    );
});

afterEach(async() => {
    await queryInterface.bulkDelete(
        "user_games", {}, { truncate: true, restartIdentity: true }
    );
    await queryInterface.bulkDelete(
        "user_game_biodatas", {}, { truncate: true, restartIdentity: true }
    );
});

describe("GET /user-game-biodatas", () => {
    it("success", (done) => {
        request(app)
            .get("/user-game-biodatas")
            .set("authorization", token)
            .end((err, res) => {
                if (err) {
                    return done(err);
                }
                expect(res.status).toBe(200);
                expect(res.body).toHaveProperty("user_game_biodatas");
                expect(res.body.user_game_biodatas).toHaveProperty("id");
                expect(res.body.user_game_biodatas).toHaveProperty("fullname");
                expect(res.body.user_game_biodatas).toHaveProperty("email");
                expect(res.body.user_game_biodatas).toHaveProperty("birth_date");
                expect(res.body.user_game_biodatas).toHaveProperty("user_game_id");
                expect(res.body.user_game_biodatas).toHaveProperty("createdAt");
                expect(res.body.user_game_biodatas).toHaveProperty("updatedAt");
                done();
            });
    });
    it("not found", (done) => {
        request(app)
            .get("/user-game-biodatas")
            .set("authorization", token2)
            .end((err, res) => {
                if (err) {
                    return done(err);
                }
                expect(res.status).toBe(404);
                expect(res.body).toHaveProperty("message");
                expect(res.body.message).toBe("User game biodata not found");
                done();
            });
    });
    it("no auth", (done) => {
        request(app)
            .get("/user-game-biodatas")
            .end((err, res) => {
                if (err) {
                    return done(err);
                }
                expect(res.status).toBe(401);
                expect(res.body).toHaveProperty("message");
                expect(res.body.message).toBe("Token not provided");
                done();
            });
    });
});

describe("POST /user-game-biodata", () => {
    it("success", (done) => {
        request(app)
            .post("/user-game-biodata")
            .set("authorization", token2)
            .send({
                fullname: "test2",
                email: "test2@gtest.com",
                birth_date: "2020-01-01",
            })
            .end((err, res) => {
                if (err) {
                    return done(err);
                }
                expect(res.status).toBe(201);
                expect(res.body).toHaveProperty("user_game_biodata");
                expect(res.body.user_game_biodata).toHaveProperty("id");
                expect(res.body.user_game_biodata).toHaveProperty("fullname");
                expect(res.body.user_game_biodata).toHaveProperty("email");
                expect(res.body.user_game_biodata).toHaveProperty("birth_date");
                expect(res.body.user_game_biodata).toHaveProperty("user_game_id");
                expect(res.body.user_game_biodata).toHaveProperty("createdAt");
                expect(res.body.user_game_biodata).toHaveProperty("updatedAt");
                done();
            });
    });
    it("user game biodata already exist", (done) => {
        request(app)
            .post("/user-game-biodata")
            .set("authorization", token)
            .send({
                fullname: "test",
                email: "test@test.com",
                birth_date: "2020-01-01",
            })
            .end((err, res) => {
                if (err) {
                    return done(err);
                }
                expect(res.status).toBe(400);
                expect(res.body).toHaveProperty("message");
                expect(res.body.message).toBe("User game biodata already exists");
                done();
            });
    });
    it("no auth", (done) => {
        request(app)
            .post("/user-game-biodata")
            .send({
                fullname: "test",
                email: "test@test.com",
                birth_date: "2020-01-01",
            })
            .end((err, res) => {
                if (err) {
                    return done(err);
                }
                expect(res.status).toBe(401);
                expect(res.body).toHaveProperty("message");
                expect(res.body.message).toBe("Token not provided");
                done();
            });
    });
});

describe("PUT /user-game-biodata/:id", () => {
    it("success", (done) => {
        request(app)
            .put("/user-game-biodata/1")
            .set("authorization", token)
            .send({
                fullname: "test2",
                email: "test2@test.com",
                birth_date: "2020-01-01",
            })
            .end((err, res) => {
                if (err) {
                    return done(err);
                }
                expect(res.status).toBe(200);
                expect(res.body).toHaveProperty("user_game_biodata");
                expect(res.body.user_game_biodata).toHaveProperty("id");
                expect(res.body.user_game_biodata).toHaveProperty("fullname");
                expect(res.body.user_game_biodata).toHaveProperty("email");
                expect(res.body.user_game_biodata).toHaveProperty("birth_date");
                expect(res.body.user_game_biodata).toHaveProperty("user_game_id");
                expect(res.body.user_game_biodata).toHaveProperty("createdAt");
                expect(res.body.user_game_biodata).toHaveProperty("updatedAt");
                done();
            });
    });
    it("not found", (done) => {
        request(app)
            .put("/user-game-biodata/100")
            .set("authorization", token)
            .send({
                fullname: "test2",
                email: "test2@test.com",
                birth_date: "2020-01-01",
            })
            .end((err, res) => {
                if (err) {
                    return done(err);
                }
                expect(res.status).toBe(404);
                expect(res.body).toHaveProperty("message");
                expect(res.body.message).toBe("User game biodata not found");
                done();
            });
    });
    it("no auth", (done) => {
        request(app)
            .put("/user-game-biodata/1")
            .send({
                fullname: "test2",
                email: "test2@test.com",
                birth_date: "2020-01-01",
            })
            .end((err, res) => {
                if (err) {
                    return done(err);
                }
                expect(res.status).toBe(401);
                expect(res.body).toHaveProperty("message");
                expect(res.body.message).toBe("Token not provided");
                done();
            });
    });
});

describe("DELETE /user-game-biodata/:id", () => {
    it("success", (done) => {
        request(app)
            .delete("/user-game-biodata/1")
            .set("authorization", token)
            .end((err, res) => {
                if (err) {
                    return done(err);
                }
                expect(res.status).toBe(200);
                expect(res.body).toHaveProperty("message");
                expect(res.body.message).toBe("User game biodata successfully deleted");
                done();
            });
    });
    it("not found", (done) => {
        request(app)
            .delete("/user-game-biodata/100")
            .set("authorization", token)
            .end((err, res) => {
                if (err) {
                    return done(err);
                }
                expect(res.status).toBe(404);
                expect(res.body).toHaveProperty("message");
                expect(res.body.message).toBe("User game biodata not found");
                done();
            });
    });
    it("no auth", (done) => {
        request(app)
            .delete("/user-game-biodata/1")
            .end((err, res) => {
                if (err) {
                    return done(err);
                }
                expect(res.status).toBe(401);
                expect(res.body).toHaveProperty("message");
                expect(res.body.message).toBe("Token not provided");
                done();
            });
    });
});