const nodeMailer = require("nodemailer");
require("dotenv").config();

module.exports = async(from, to, subject, text, html) => {
    try {
        const transporter = nodeMailer.createTransport({
            host: "smtp.mailtrap.io",
            port: 2525,
            secure: false,
            auth: {
                user: process.env.MAIL_USER,
                pass: process.env.MAIL_PASS,
            },
        });
        const info = await transporter.sendMail({
            from: from,
            to: to,
            subject: subject,
            text: text,
            html: html,
        });
        return info;
    } catch (err) {
        console.log(err);
    }
};